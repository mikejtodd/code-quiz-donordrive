var activityFeed = document.querySelector('.feed__list');

var activityItems = [
    {
        badge: 'recruitment',
        title: 'New Team Badge!',
        message: 'Recruit 10 Team Members',
        createdDateTime: '2019-10-08T12:00:00Z'
    },
    {
        badge: '1k',
        title: 'New Badge!',
        message: 'Raise $1K!',
        createdDateTime: '2019-10-09T14:30:00Z'
    },
    {
        amount: '50',
        title: 'Ben Clayton',
        message: '"Way to go, guy! You really are the champion of Rocket League."',
        createdDateTime: '2019-10-12T16:00:00Z'
    }
];

for (var i = 0, l = activityItems.length; i < l; i++) {
    addItemToActivityFeed(activityItems[i]);
}

var newActivityItems = [
    {
        badge: 'spread-the-news',
        title: 'New Badge!',
        message: 'Email 5 or More Contacts'
    },
    {
        amount: '7K',
        title: 'Jeni',
        message: 'Woof! Ruff ruff, bark. Ergh. Wooooof.'
    },
    {
        badge: 'super-star',
        title: 'New Badge!',
        message: 'Raise $5K!'
    }
];

var newActivityInterval = setInterval(function() {
    getNewActivity();
}, 5000);

function getNewActivity() {
    var newActivity = newActivityItems[0];
    
    addItemToActivityFeed(newActivity, true);
    
    newActivityItems.shift();
    
    if (newActivityItems.length === 0) {
        clearInterval(newActivityInterval);
    }
}

function addItemToActivityFeed(item, isNew) {
    var newListItem = document.createElement('li');
    
    activityFeed.insertBefore(newListItem, activityFeed.querySelector('li:first-of-type'));
    var newEmptyListItem = activityFeed.querySelector('li:first-of-type');
    addItemInnerHTML(item, newEmptyListItem);
    
    if (isNew) {
        activityFeed.classList.add('animate');
        
        var allActivityItems = activityFeed.querySelectorAll('li');
        
        var activityItemsCount = allActivityItems.length;
        var percentageWidth = 100 / activityItemsCount;
        
        allActivityItems[0].style.width = allActivityItems[1].offsetWidth + 'px';
        
        setTimeout(function() {
            activityFeed.classList.add('magic');
            activityFeed.classList.remove('animate');
            
            for (var i = 0, l = activityItemsCount; i < l; i++) {
                allActivityItems[i].style.width = percentageWidth + '%';
            }
            
            activityFeed.style.width = ((activityItemsCount / 3) * 100) + '%';
            
            setTimeout(function() {
                activityFeed.classList.remove('magic');
            }, 1000);
        }, 1000);
    }
}

function addItemInnerHTML(item, newEmptyListItem) {
    var badgeContent = '';
    
    if (item.amount) {
        badgeContent = 
        '<p>' + 
            '<strong>' + 
                '$' + item.amount + 
            '</strong>' + 
        '</p>';
    } else if (item.badge) {
        badgeContent = 
        '<img src="images/icons/badges/' + item.badge + '.png" alt="">';
    }
    
    var titleContent = '';
    
    if (item.title) {
        titleContent = 
        '<p class="description__title">' + 
            '<strong>' + 
            item.title + 
            '</strong>' + 
        '</p>';
    }
    
    var messageContent = '';
    
    if (item.message) {
        messageContent =
        '<p class="description__message">' + 
            item.message + 
        '</p>';
    }
    
    var createdDateTimeContent = '';
    
    if (item.createdDateTime) {
        createdDateTimeContent = 
        '<p class="description__time">' + 
            convertToRelativeTime(item.createdDateTime) + 
        '</p>';
    } else {
        createdDateTimeContent = 
        '<p class="description__time">' + 
            'Just Now' + 
        '</p>';
    }
    
    newEmptyListItem.innerHTML = 
    '<div class="item__badge">' + 
        badgeContent + 
    '</div>' + 
    '<div class="item__description">' + 
        titleContent + 
        messageContent + 
        createdDateTimeContent + 
    '</div>';
}

function convertToRelativeTime(createdDateTime) {
    var currentDateTime = new Date();
    var parsedDateTime = Date.parse(createdDateTime);
    var secondsSinceCreation = (currentDateTime.getTime() - parsedDateTime) / 1000;
    
    if (secondsSinceCreation < 60) {
        return 'Just Now';
    } else if (secondsSinceCreation <= 86400) {
        var hoursSingularPlural = ' hours';
        
        if (secondsSinceCreation < 3600) {
            hoursSingularPlural = 'hour';
        }
        
        return parseInt(secondsSinceCreation / 3600) + hoursSingularPlural;
    } else {
        var daysSingularPlural = ' days';
        
        if (secondsSinceCreation < 172800) {
            daysSingularPlural = ' day';
        }
        
        return parseInt(secondsSinceCreation / 86400) + daysSingularPlural;
    }
}
