var navButtons;
var tabs;
var buttonsBoth;

var navToggle;
var navCancel;

var streamChat;

getNavigationDomElements();
setupNavigationClickListeners();
setDefaultEmbedView();

function getNavigationDomElements() {
    navButtons = document.querySelector('.nav__buttons');
    tabs = navButtons.querySelectorAll('a');
    buttonsBoth = navButtons.querySelector('.buttons__both');
    
    navToggle = document.querySelector('.nav__toggle');
    navCancel = document.querySelector('.nav__cancel');
    
    streamChat = document.querySelector('.streamer__stream-chat');
}

function setupNavigationClickListeners() {
    for (var i = 0, l = tabs.length; i < l; i++) {
        tabs[i].addEventListener('click', toggleTab);
    }
    
    navToggle.addEventListener('click', toggleNav);
    navCancel.addEventListener('click', toggleNav);
}

function toggleTab(event) {
    event.preventDefault();
    
    removeTabActiveStates();
    
    this.parentNode.classList.add('active');
    
    document.body.classList.remove('overlay');
    
    var tab = this.getAttribute('href').replace('/','');
    
    streamChat.setAttribute('class', 'streamer__stream-chat ' + tab);
}

function toggleNav(event) {
    event.preventDefault();
    
    if (document.body.getAttribute('class') === 'overlay') {
        document.body.classList.remove('overlay');
    } else {
        document.body.classList.add('overlay');
    }
}

var previousWindowWidth;

window.onresize = setDefaultEmbedView;

function setDefaultEmbedView() {
    if (previousWindowWidth !== window.innerWidth > 992) {
        if (window.innerWidth > 992) {
            removeTabActiveStates();
            buttonsBoth.classList.add('active');
            
            streamChat.setAttribute('class', 'streamer__stream-chat');
        } else {
            removeTabActiveStates();
            
            streamChat.setAttribute('class', 'streamer__stream-chat stream');
        }
    }
    
    previousWindowWidth = window.innerWidth > 992;
}

function removeTabActiveStates() {
    for (var i = 0, l = tabs.length; i < l; i++) {
        tabs[i].parentNode.classList.remove('active');
    }
}
