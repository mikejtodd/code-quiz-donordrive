'use strict';

var gulp = require('gulp');

// HTML-related
var beautifyCode = require('gulp-beautify-code');
var twig = require('gulp-twig');

// CSS-related
var less = require('gulp-less');
var postcss = require('gulp-postcss');
var autoprefixer = require('autoprefixer');
var cleancss = require('gulp-clean-css');

// JS-related
var concat = require('gulp-concat');
var minify = require('gulp-minify');
var eslint = require('gulp-eslint');

// Utility-related
var sourcemaps = require('gulp-sourcemaps');
var connect = require('gulp-connect');
var open = require('gulp-open');

var roots = {
    assets: './code/src/assets',
    dist: './code/dist',
    pages: './code/src/pages',
    src: './code/src'
};

var javascript = [
    roots.assets + '/javascript/vendor/twitch.min.js',
    roots.assets + '/javascript/navigation.js',
    roots.assets + '/javascript/embeds.js',
    roots.assets + '/javascript/activity-feed.js'
];

// Creates JS sourcemaps, concatenates JS files into one based on array above, minifies JS
gulp.task('js', function(done) {
    gulp.src(javascript)
        .pipe(eslint({
            configFile: 'package.json'
        }))
        .pipe(eslint.format())
        .pipe(sourcemaps.init())
        .pipe(concat('main.js'))
        .pipe(sourcemaps.write('./'))
        .pipe(minify({
            ext: {
                min: '.min.js'
            }
        }))
        .pipe(gulp.dest(roots.dist + '/js'))
        .pipe(connect.reload());

    done();
});

// Creates CSS sourcemaps, adds prefixes, lints CSS
gulp.task('less', function(done) {
    var plugins = [
        autoprefixer({grid: true})
    ];

    gulp.src(roots.assets + '/styles/main.less')
        .pipe(sourcemaps.init())
        .pipe(less())
        .pipe(postcss(plugins))
        .pipe(cleancss({compatibility: 'ie11'}))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(roots.dist + '/css'))
        .pipe(connect.reload());

    done();
});

// Converts only the index.twig file to HTML, properly indents HTML, corrects relative paths
gulp.task('twig', function(done) {
    gulp.src(roots.pages + '/index.twig')
        .pipe(twig())
        .pipe(beautifyCode())
        .pipe(gulp.dest(roots.dist))
        .pipe(connect.reload());

    done();
});

// Moves images from src folder to dist folder
gulp.task('images', function(done) {
    gulp.src(roots.assets + '/images/**/*')
        .pipe(gulp.dest(roots.dist + '/images'))
        .pipe(connect.reload());

    done();
});

// Runs a server to static HTML files, and sets up watch tasks
gulp.task('server', function(done) {
    connect.server({
        root: roots.dist,
        livereload: true
    });

    gulp.src(__filename)
        .pipe(open({uri: 'http://0.0.0.0:8080'}));

    gulp.watch((roots.src + '/**/*.twig'), gulp.series('twig'));
    gulp.watch((roots.assets + '/javascript/**/*.js'), gulp.series('js'));
    gulp.watch((roots.assets + '/styles/**/*.less'), gulp.series('less'));
    gulp.watch((roots.assets + '/images/**/*'), gulp.series('images'));

    done();
});

gulp.task('build', gulp.series('twig', 'js', 'less'));

gulp.task('default', gulp.series('server'));
