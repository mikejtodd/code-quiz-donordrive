# Code Quiz for DonorDrive

Code quiz for DonorDrive's Front-End Developer position. Features a video player that includes an embed from Twitch.

## Project setup

This project uses Gulp to concatenate and minify JS, compile LESS to CSS, and convert Twig templates into HTML.

To install node modules, run: `npm install`

To start a server to the static HTML files and watch src files for changes, run: `gulp`
